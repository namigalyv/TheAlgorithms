#include<iostream>

using namespace std;

template <typename Object>
class Queue
{
  private:
    int front, rear, capacity, length;
    Object *queue;

  void reverse(int newCapacity)
  {
      Object *temp = new Object[newCapacity];
      for(int i=front, j=0; j<length; j++, i++)
      {
          temp[j] = queue[i];
      }
      capacity = newCapacity;
      delete[] queue;
      queue = temp;
      front = 0;
      back = length - 1;
  }
  public:
    Queue(int c)
    {
        capacity = 1;
        front = 0;
        rear = -1;
        length = 0;
        queue = new Object[capacity];
    }

    void clear()
    {
        front = 1;
        rear = -1;
        length = 0;
    }

    int count() const
    {
        return length;
    }

    bool isEmpty() const
    {
        return length == 0;
    }

    const Object& peek() const
    {
        //if(isEmpty()) throw queueEmpty
        return queue[front];
    }

    void enqueue(const Object& item)
    {
        if(rear == capacity-1)
          reverse(2*capacity);
        rear++;
        queue[rear] = item;
        length++;
    }

    void dequeue()
    {
        //if(isEmpty()) throw queueEmpty
        front++;
        length--;
    }

    ~Queue()
    {
        delete[] Object;
    }
};

struct Student
{
    int no;
    string nameSurname;
    Student(int number, string name)
    {
        no = number;
        nameSurname = name;
    }
    friend ostream& operator<<(ostream& screen, Student& right)
    {
        screen << right.no << " " << right.nameSurname << endl;
        return screen;
    }
};

int main()
{
    Queue<int> &queue = new Queue<int>();

    queue->enqueue(15);
    queue->enqueue(16);
    queue->enqueue(17);
    queue->enqueue(18);
    queue->dequeue();
    queue->enqueue(19);
    queue->enqueue(20);
    queue->enqueue(21);

    queue->dequeue();
    queue->dequeue();
    queue->dequeue();

    queue->enqueue(1);
    queue->enqueue(2);
    queue->enqueue(3);
    queue->enqueue(4);
    queue->enqueue(5);
    cout << queue->peek() <<endl<<endl;
    delete queue;

    Queue<Student*> &queue = new Queue<Student*>();
    Student *david = new Student(224, "David Fowler");
    Student *malan = new Student(100, "David J.Malan");
    Student *durant = new Student(500, "Kevin Durant");

    queue->enqueue(david);
    queue->enqueue(malan);
    cout << *(queue->peek()) << endl;
    queue->dequeue();
    cout << *(queue->peek()) << endl;
    queue->enqueue(durant);
    queue->enqueue(david);
    cout << *(queue->peek()) << endl;
    delete david;
    delete malan;
    delete durant;
    delete queue;
    return 0;
}
