#include<iostream>

using namespace std;

struct Node
{
    int data;
    Node *next;
};

class Stack
{
  private:
    Node *top;

  public:
    Stack()
    {
        top = NULL;
    }

    void push(int data)
    {
        Node *temp = new Node;

        temp->data = data;
        temp->next = top;
        top = temp;
    }

    int isEmpty()
    {
        return top == NULL;
    }

    int peek()
    {
        if(!isEmpty())
          return top->data;
        else
          exit(1);
    }

    void pop()
    {
        Node *temp = new Node;

        temp = top;
        top = top->next;
        temp->next = NULL;

        free(temp);
    }

    void display()
    {
       Node *temp = new Node;

       temp = top;
       while(temp != NULL)
       {
          cout << temp->data << " ";
          temp = temp->next;
       }
    }
};

int main()
{
    Stack object = new Stack;
    object.push(11);
    object.push(22);
    object.push(14);
    object.push(23);
    object.push(7);

    object.display();

    object.pop();
    object.pop();

    object.display();

    return 0;
}
