#include<iostream>

using namespace std;

template <typename Object>
class Stack
{
  private:
    Object *elements;
    int head;
    int elementCount;
    int capacity;

    bool isFully()
    {
        return elementCount == capacity;
    }

    void makeNewArea(int count)
    {
        Object *temp = new Object[count];
        for(int i = 0; i < elementCount; i++)
          temp[i] = elements[i];
        if(elements != NULL)
          delete [] elements;

        elements = temp;
        elementCount = count;
    }
  public:
    Stack()
    {
        elements = NULL;
        head = -1;
        elementCount = 0;
        capacity = 0;
    }

    bool isEmpty() const
    {
        return head == -1;
    }

    void push(const Object &element)
    {
        if(isFully)
            makeNewArea(max(1,2*capacity));
        head++;
        elements[head] = element;
        elementCount++;
    }

    void pop() // delete last element
    {
        if(isEmpty)
          //throw elements not found
        head--;
        elementCount--;
    }

    const Object& top()const // get first element
    {
        if(isEmpty)
          //throw elements not found
        return elements[head];
    }

    void makeEmpty() // change stack first versiyon
    {
        head = -1;
        elementCount = 0;
    }

    ~Stack()
    {
        if(elements != NULL)
          delete [] elemets;
        elements = NULL;
    }
}

int main()
{
    Stack<int> *stack = new Stack<int>();
    stack->push(25);
    stack->push(32);
    stack->push(68);
    stack->push(74);
    stack->pop();
    cout<<stack->top();
    delete stack;
    cout<<endl;

    Stack<string> *stck = new Stack<string>();
    stck->push("Mehmet");
    stck->push("Veli");
    stck->push("Ahmet");
    stck->push("Sakarya");
    stck->pop();
    cout<<stck->top();
    delete stck;
    return 0;
}
