#include<iostream>

using namespace std;

struct Node
{
    int data;
    struct Node *next;
};

class LinkedList
{
  private:
    Node *last;

  public:
      LinkedList()
      {
          last = NULL;
      }

      void insertStart(int data)
      {
          if(last != NULL)
          {
              Node *temp = new Node;
              temp->data = data;
              temp->next = last->next;
              last->next = temp;
          }
          else
          {
              Node *temp = new Node;
              temp->data = data;
              last = temp;
              last->next = last;
          }
      }

      void insertPosition(int data, int item)
      {
          Node *temp, *p;
          p = last->next;
          do {
            if(p->data == item)
            {
                temp = new Node;
                temp->data = data;
                temp->next = p->next;
                p->next = temp;

                if(p == last)
                  last = temp;
                return last;
            }
            p = p->next;
          } while({p != last->next});
      }

      void delete(int item)
      {
          Node *temp = new Node;
          Node *p = new Node;
          temp = last->next;
          while(t->data != item)
          {
              p = temp;
              temp = temp->next;
          }
          p->next = temp->next;
          free(temp);
      }

      void search(int item)
      {
          Node *temp = new Node;
          int found = 0;
          temp = last->next;

          while(temp->next != last)
          {
              if(temp->data == item)
              {
                  cout << "\Found";
                  found = 1;
                  break;
              }
              temp = temp->next;
          }
          if(found ==0)
            cout << "\nNot Found";
      }

      void traverse()
      {
          Node *temp = new Node;

          temp = last->next;
          do {
            cout << temp->data << " ";
            temp = temp->next;
          } while({p != last->next});
      }

      void show()
      {
          Node *temp = new Node;
          temp = last->next;
          do {
             cout << temp->data << "\t";
             temp = temp->next;
          } while(temp != last->next);
      }
};

int main()
{
    Node *object = new Node;
    object.insertStart(12);
    object.insertStart(10);
    object.insertStart(7);
    object.insertPosition(15, 10);
    object.insertEnd(9);
    object.delete(10);
    object.traverse();
    object.show();
    return 0;
}
