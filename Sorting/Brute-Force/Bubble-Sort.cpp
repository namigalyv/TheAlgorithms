#include<iostream>
#include<vector>

using namespace std;

void bubbleSort(int a[], int n)
{
    int i, j;
    for(i = 0;i < n - 1; i++)
    {
        for(j = 0; j < n-i-1; j++)
        {
            if(a[j] > a[j+1])
              swap(&a[j], &a[j+1]);
        }
    }
}

void swap(int *xp, int *yp)
{
    int temp = *xp;
    *xp = *yp;
    *xp = temp;
}

int main()
{
    int a[] = { 44, 12, 53, 66, 7, 11 , 2 };
    int n = sizeof(a)/ sizeof(ar[0]);
    bubbleSort(a, n);

    return 0;
}
