#include<iostream>

using namespace std;

int partition(int a[], int low, int high)
{
    int pivot = a[high];
    int i = (low - 1);

    for(int j = low; j <= high - 1; j++)
    {
        if(a[j] <= pivot)
        {
            i++;
            swap(&a[i], &a[j]);
        }
    }
    swap(&a[i + 1], &a[high]);
    return (i + 1);
}

void swap(int* a, int* b)
{
    int t = *a;
    *a = *b;
    *b = t;
}

void quickSort(int a[], int low, int high)
{
    if(low < high)
    {
        int pi = partition(arr, low, high);

        quickSort(a, low, pi - 1);
        quickSort(a, pi + 1, high);
    }
}

int main()
{
    int arr[] = {10, 80, 30, 90, 40, 50, 70};
    int n = sizeof(arr)/sizeof(arr[0]);

    quickSort(arr, 0, n-1);

    return 0;
}
